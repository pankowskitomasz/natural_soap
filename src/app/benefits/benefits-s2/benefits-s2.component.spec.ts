import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsS2Component } from './benefits-s2.component';

describe('BenefitsS2Component', () => {
  let component: BenefitsS2Component;
  let fixture: ComponentFixture<BenefitsS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
