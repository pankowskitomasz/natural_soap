import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsS3Component } from './benefits-s3.component';

describe('BenefitsS3Component', () => {
  let component: BenefitsS3Component;
  let fixture: ComponentFixture<BenefitsS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
