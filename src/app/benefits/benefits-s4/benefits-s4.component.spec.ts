import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsS4Component } from './benefits-s4.component';

describe('BenefitsS4Component', () => {
  let component: BenefitsS4Component;
  let fixture: ComponentFixture<BenefitsS4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsS4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsS4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
