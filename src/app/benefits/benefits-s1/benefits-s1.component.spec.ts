import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsS1Component } from './benefits-s1.component';

describe('BenefitsS1Component', () => {
  let component: BenefitsS1Component;
  let fixture: ComponentFixture<BenefitsS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
