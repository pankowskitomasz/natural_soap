import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitsRoutingModule } from './benefits-routing.module';
import { BenefitsComponent } from './benefits/benefits.component';
import { BenefitsS1Component } from './benefits-s1/benefits-s1.component';
import { BenefitsS2Component } from './benefits-s2/benefits-s2.component';
import { BenefitsS3Component } from './benefits-s3/benefits-s3.component';
import { BenefitsS4Component } from './benefits-s4/benefits-s4.component';
import { BenefitsS5Component } from './benefits-s5/benefits-s5.component';


@NgModule({
  declarations: [
    BenefitsComponent,
    BenefitsS1Component,
    BenefitsS2Component,
    BenefitsS3Component,
    BenefitsS4Component,
    BenefitsS5Component
  ],
  imports: [
    CommonModule,
    BenefitsRoutingModule
  ]
})
export class BenefitsModule { }
