import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsS5Component } from './benefits-s5.component';

describe('BenefitsS5Component', () => {
  let component: BenefitsS5Component;
  let fixture: ComponentFixture<BenefitsS5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsS5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsS5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
