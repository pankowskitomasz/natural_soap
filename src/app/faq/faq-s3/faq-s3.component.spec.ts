import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqS3Component } from './faq-s3.component';

describe('FaqS3Component', () => {
  let component: FaqS3Component;
  let fixture: ComponentFixture<FaqS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaqS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
